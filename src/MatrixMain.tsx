import React, { useState } from "react";
import { Table } from "antd";
import computedColumns from "./MatrixColumns/MatrixTasksColumns";
import columns1 from "./MatrixColumns/MatrixStudentsColumns";
import "./styles.css";
import {ITasks} from "./App"

export interface IMatrixData {
  userId: number;
  user: any;
  tasks: ITasks[];
}

export interface IProps {
  onUserAction: (userIds: number[], actionName: string, cell: ITasks) => void;
  data: IMatrixData[];
}


function MatrixMain({ onUserAction, data }: IProps) {
  const countStud = data.length;

  const [chekedIds, setChecked] = useState([]);
  const onUserAct = (actionName: string, cell: ITasks) => {
    onUserAction(chekedIds, actionName, cell);
  };
  
  const rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, "selectedRows: ", selectedRows);
      setChecked(selectedRows.map((item: any) => item.userId));
    },
    getCheckboxProps: (record: any) => {
      return {
        name: record.name,
      };
    },
  };

  //ячейки с задачами
  const computedColumns1 = computedColumns(data, onUserAct);
  const columnsLeft = columns1(countStud);
  const columns = [...columnsLeft, ...computedColumns1];

  return (
    <Table
      dataSource={data}
      columns={columns}
      scroll={{ x: 1500 }}
      rowKey={"userId"}
      rowSelection={rowSelection}
    ></Table>
  );
}

export default MatrixMain;
