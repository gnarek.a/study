import React from "react"
import { ITasks } from "../App";

export interface IList {
  text: string
}

export interface IPopoverList {
  cell?: ITasks;
  onUserAction: (actionName: string, cell: ITasks) => void;
  userId: number | '*';

}

let arrTextList = [
  { text: "Перейти к заданию" },
  { text: "Перейти к уроку" },
  { text: "Назначить" },
  { text: "Назначить как ДЗ" },
  { text: "Назначить срок выполнения" },
  { text: "Отменить назначение" },
];

export default function TaskPopoverList(props: IPopoverList) {
  const { cell, onUserAction, userId } = props;

  // const {cellView, setCellView} = useState('');

  // changeCellView () => {
  //   setCellView (cellView);
  // }

  if (!cell) {
    return <div>NOT FOUND</div>
  }

  const list1 = arrTextList.map((item, index) => {

    // console.log('item', item);
    
    return (<li key={index}>
      <a href="/#"
        onClick={() => {
          onUserAction(item.text, cell);
        }}
        key={cell.id}
      >
        {item.text}
      </a>
    </li>)
  })

  return (
    <ul>{list1}</ul>
  )
}