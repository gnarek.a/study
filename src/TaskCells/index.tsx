import React from "react";
import { ITasks } from "../App";
import TaskPopover from "./TaskPopover";
import "./styles.css";
import { useDispatch } from "react-redux";
import { TASKS_ASSIGN } from "../actions";

export interface ITaskCells {
  cell?: ITasks;
  onUserAction: (actionName: string, cell: ITasks) => void;
  userId: number | "*";
  // userIds: [];
  children: any;
}

export interface IList {
  text: string;
}

//прописать условие для одного юзера и передавать обычный поповер

const TaskCells = (props: ITaskCells) => {
  const { cell, onUserAction, userId } = props;
  const dispatch = useDispatch();

  return (
    <div>
      {cell && (
        <TaskPopover
          cell={cell}
          onUserAction={() => dispatch(TASKS_ASSIGN(userId, cell))}
          userId={userId}
        >
          {props.children}
        </TaskPopover>
      )}
    </div>
  );
};

export default TaskCells;
