import React, { useState } from "react";
import { ITaskCells } from "./index";
import { Popover, Tooltip } from "antd";
import LessonTitle from "../LessonTitle";
import TaskPopoverList from "./TaskPopoverList";


export default function TaskPopover(props: ITaskCells) {
  const { cell, onUserAction, userId } = props;

  const [, setVisible] = useState<boolean>(false);
  const onChange = (visible: boolean, id: number) => {
    // console.log("ID FETCH", id);
    setVisible(visible);
  };

  if (!cell) {
    return <div>NOT FOUND</div>
  }

 
  return (
    <Popover content={
      <TaskPopoverList onUserAction={onUserAction} cell={cell} userId={userId}/>
    }
      trigger="click"
      onVisibleChange={visible => onChange(visible, cell.id)}
    >
      <Tooltip title={<LessonTitle />}>
        {props.children}
      </Tooltip>
    </Popover>
  )
}