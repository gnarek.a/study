import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { DATA_LOADED, DATA_LOADING } from "./actions";

//пользовательский хук должен начинаться с use
export default function useGetDataHook(arr: any[]) {
  const [data, setData] = useState(arr);
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    dispatch(DATA_LOADING)
    fetch("https://raw.githubusercontent.com/SvetlanaB/data-api/master/data.json").then(results =>
      results.json().then(data => {
        setData(data);
        setLoading(false);
        dispatch<any>(DATA_LOADED(data))
      })
    );
  }, []);

  return {data, loading}
}
