import React from 'react';
import { Avatar } from 'antd';
import { Icon } from 'antd';

import "./styles.css";

export interface IStudentProps {
    userId: number
    user: any
}

function Student(props: IStudentProps) {
    const { userId, user } = props
    if (!user) {
        return <div>USER NOT fOUND</div>
    }

    let colorAP;
    if (user.ap === 1) {
        colorAP = '#3958F4'
    } else if (user.ap === 2) {
        colorAP = '#D21A21'
    }
    const style = { backgroundColor: colorAP };

    return (
        <div className="student" key={userId}>
            <div className="flex-str student-data">
                <div className="student-avatar">
                    <Avatar icon="user" src={user.img} />
                </div>
                <div className="student-name">
                    {user.firstName} {user.lastName}
                </div>
            </div>
            <div className="flex-str student-mark">
                <div className="student-color-mark-ovz" style={style}></div>
                <Icon type="message" theme="twoTone" />
            </div>
        </div>
    )
}

export default Student