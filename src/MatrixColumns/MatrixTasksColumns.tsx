import React from "react";
import { IMatrixData } from "../MatrixMain";
import {ITasks} from "../App";
import { ColumnProps } from "antd/lib/table";
import TaskCells from "../TaskCells";

import IconQuestWithAnOpenAnswer from "../CardIcons/IconKv";
import IconZkz from "../CardIcons/IconZkz";
import IconZoo from "../CardIcons/IconZoo";


const computedColumns = (
  data: IMatrixData[],
  onUserAction: (actionName: string, cell: ITasks) => void
): ColumnProps<IMatrixData>[] =>
  data[0].tasks.map((task: ITasks) => {
    return {
      title: () => {
        switch (task.type) {
          case "Kv":
            return (
              <TaskCells onUserAction={onUserAction} userId="*" cell={task}>
                <IconQuestWithAnOpenAnswer type={task.type} />
              </TaskCells>
            );
          case "Zkz":
            return <IconZkz type={task.type} />;
          case "Zoo":
            return <IconZoo weight={task.weight} />;
          default:
            return task.type;
        }
      },
      key: task.id,
      dataIndex: task.type,
      render: (text: any, record) => {
        const cell = record.tasks.find(tsk => tsk.id === task.id);
        return (
          <TaskCells
            userId={record.userId}
            onUserAction={onUserAction}
            cell={cell}
          >

            {cell && cell.assign ? (
              <a key={cell.id} href="/#">
                {cell.assign}
              </a>
            ) : (
                <span>пусто</span>
              )}
          </TaskCells>
        );
      },
    };
  });
export default computedColumns;
//assign 