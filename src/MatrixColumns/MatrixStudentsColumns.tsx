import React from "react";
import {IMatrixData} from "../MatrixMain";
import Student from "../Student";

const columns1 = (countStud: number) => [
    //первая колонка с учениками
    {
      title: () => (
        <div>
          Учеников: <span style={{ color: "#1890FF" }}>{countStud}</span>
        </div>
      ),
      dataIndex: "name",
      key: "name",
      fixed: true,
      width: 427,
      render: (text: any, record: IMatrixData) => {
        return <Student user={record.user} userId={record.userId} />;
      },
    },
  ];

  export default columns1;