import React from "react";
import "./App.css";
import "antd/dist/antd.css";
import MatrixMain from "./MatrixMain";
import useGetDataHook from "./getDataHook";
import { useSelector } from "react-redux";

export interface ITasks {
  assign?: string;
  id: number;
  type: string;
  name: string;
  ap: number;
  weight: number;
}

function App() {
  const { loading } = useGetDataHook([]);
  const load: any = useSelector((state:any) => state.loading)
  const data: any = useSelector((state:any) => state.data)
 
  const onUserAction = (userId: number[], actionName: string, cell: ITasks) => {
    console.log({ userId, actionName, cell });
    if (actionName === "Перейти к уроку") {
      console.log("HEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
    }
    if (actionName === "Назначить как ДЗ") {
      // changeCellView("ДЗ", cell);
      // console.log(cellState);
      // setCellState(cellState);
      cell.assign = "ДЗ";
    }
  };

  //надо превратить actionName в функцию

  if (load) {
    return <div>...loading</div>;
  }
 
  return (
    <div className="App">
      <MatrixMain data={data} onUserAction={onUserAction} />
    </div>
  );
}

export default App;