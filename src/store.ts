import { createStore } from "redux";
import { ACTION_DATA_LOADING, ACTION_DATA_LOADED, SET_TASK_ALL } from "./actions";
import { func } from "prop-types";

declare global {
  interface Window {
    // tslint:disable-next-line: no-any
    __REDUX_DEVTOOLS_EXTENSION__?: any;
  }
}

function dataReducer(state = { data: [], loading: true }, action: any) {
  switch (action.type) {
    case ACTION_DATA_LOADING: {
      return { ...state, loading: true };
    }
    case ACTION_DATA_LOADED: {
      return { ...state, data: action.payload, loading: false };
    }
    case SET_TASK_ALL: {
      const { userIds, task } = action.payload;
      console.log('action.payload', action.payload);
      
      const data = assignTasks(state.data, userIds, task);
      console.log('data', data);
      
      return {...state, data};
    }
    default:
      return state;
  }
}

function assignTasks(state: any, userIds: any, task: any) {
    console.log('state', state);
    
  return state.map((data: any) => {
    if (userIds === "*") {
      data.tasks.map((tsk: any) => {
        if (tsk.id === task.id) {
          tsk.assign = "Назначить"
        }
        return tsk
      });
      return data
    }
  });
}

let store = createStore(
  dataReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
