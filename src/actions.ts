import { ITasks } from "./App";

export const ACTION_DATA_LOADING = "DATA_LOADING";
export const ACTION_DATA_LOADED = "DATA_LOADED";
export const SET_TASK_ALL = "SET_TASK_ALL";

// ======== ACTION =========

export const DATA_LOADING = { type: ACTION_DATA_LOADING };
export const DATA_LOADED = (data: any) => ({ type: ACTION_DATA_LOADED, payload: data });
export const TASKS_ASSIGN = (userIds: any, task: ITasks) => ({
  type: SET_TASK_ALL,
  payload: {
    userIds,
    task,
  },
});
